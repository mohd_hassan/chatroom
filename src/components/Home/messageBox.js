import React, { Component } from 'react';

import { FormControl, Button } from 'react-bootstrap';
import firebase, { database } from '../../firebase/firebase';
import classes from './home.css';
import Spinner from '../Spinner/spinnerMsg';
import SpinnerMsg from '../Spinner/extraSpinner/spinner';

class messageBox extends Component {

  state = {
    loggedIn: false,
    message: '',
    chat: [],
    userName: '',
    loading: true
  }

  scrollToBottom = () => {
    this.messagesEnd.scrollIntoView({ behavior: "smooth" });
  }

  messagehandler = e => {
    this.setState({ message: e.target.value })
  }

  addMessage = (message,target) => {
    if (message !== '') {
      this.state.chat.push(`${this.state.userName}: ${message}`);
    }
    this.setState({ message: '' });
    const { chat } = this.state;
    database.child(this.props.location.state.room).update(
      chat
    );
    this.scrollToBottom();
  }

  addEnterMessage = (target) => {
    if (target.charCode === 13)
      this.addMessage(this.state.message);
  }

  componentDidMount() {
    this.mounted = true;
    firebase.auth().onAuthStateChanged(user => {
      if (this.mounted) {
        if (user && this.props.location.state) {
          this.setState({ loggedIn: true, userName: user.displayName });
        }
        else {
          this.props.history.push('/home');
        }
      }
      database.child(this.props.location.state.room)
        .on("value", snap => {
          if (this.mounted) {
            this.setState({ chat: snap.val(), loading: false });
            this.scrollToBottom();
          }
        })
      firebase.database().ref(this.props.location.state.room)
        .orderByKey()
        .limitToLast(5)
        .once('value')
        .then((snapshot) => { console.log('ye',snapshot.val())})
        .catch((error) => { error });
    });
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  signOut = () => {
    firebase.auth().signOut();
    this.props.history.push({
      pathname: '/login',
      state: { room: null, loggedIn: false }
    });
  }

  back = () => {
    this.props.history.push('/home');
  }

  render() {
    const { userName } = this.state;
    const userLength = userName.length + 1;

    // const userlength = data.lastIndexOf(this.state.userName);

    const chat = this.state.chat.map((data, index) => {
      if (index === 0)
        return null;
      if (data.indexOf(userName) === 0) {
        return (
          <li key={index} style={{ display: 'block' }}>
            <div className={classes.myMsg}><p>{data.substring(userLength)}</p></div>
          </li>);
      }
      return (
        <li key={index} >
          <div className={classes.othersMsg}><p>{data}</p></div>
        </li>);
    });
    return (
      <div>{
        this.state.loggedIn ?
          <div>
            <header style={{ height: '80px', padding: '20px' }}>
              <Button
                bsStyle='danger'
                className={classes.btnDanger}
                onClick={this.signOut} >SignOut</Button>
              <Button
                bsStyle='warning'
                onClick={this.back}
                className={classes.btnWarning}
              >Back</Button>
            </header>
            <div className={classes.chatContainer}>
              <h3>Welcome to ChatRoom</h3>
              <div className={classes.chatBox}>
                <ul className={classes.messageList}>
                  {this.state.loading ? <Spinner /> : chat}
                  <li ref={(el) => { this.messagesEnd = el; }} style={{ visibility: 'hidden' }}>,<div className={classes.myMsg}><p>x</p></div></li>
                </ul>
              </div>
              <FormControl
                onKeyPress={this.addEnterMessage}
                className={classes.FormControl}
                type="text"
                value={this.state.message}
                placeholder="Enter text"
                onChange={this.messagehandler} />
              <Button bsStyle='success' className={classes.btnSuccess} onClick={() => this.addMessage(this.state.message)}>Send</Button>
            </div>
          </div> : <SpinnerMsg />
      }
      </div>
    );

  }
}

export default messageBox;