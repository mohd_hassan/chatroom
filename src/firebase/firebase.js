import firebase from 'firebase';

var config = {
  apiKey: "AIzaSyAHuGGHXenhNcB7zsHChAons4saOdIEnXM",
  authDomain: "reactchatapp1.firebaseapp.com",
  databaseURL: "https://reactchatapp1.firebaseio.com",
  projectId: "reactchatapp1",
  storageBucket: "",
  messagingSenderId: "989027360731"
};
const firebaseApp = firebase.initializeApp(config);
export const database = firebase.database().ref('Rooms');

export default firebaseApp;