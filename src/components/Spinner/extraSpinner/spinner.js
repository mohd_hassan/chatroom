import React from 'react';

import classes from './spinner.css';

const spinner = () => (<div className={classes.Loader}><div></div><div></div><div></div><div></div></div>);

export default spinner;