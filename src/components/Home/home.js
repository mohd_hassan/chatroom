import React from 'react';

import { FormControl, Button, Alert } from 'react-bootstrap';
import firebase, { database } from '../../firebase/firebase';
import classes from './home.css';

class Home extends React.Component {

  state = {
    loggedIn: false,
    selectRoom: '',
    createRoom: '',
    alertCreate: '',
    alertJoin: '',
  }

  componentWillMount() {
    this.mounted = true;
    firebase.auth().onAuthStateChanged(user => {
      if (user)
        this.setState({ loggedIn: true });
      else
        this.props.history.push('/login');
    })
  }

  getValidationState() {
    return 'success';
  }

  selectRoomHandler = (e) => {
    this.setState({ selectRoom: e.target.value })
  }

  createRoomHandler = (e) => {
    this.setState({ createRoom: e.target.value })
  }

  createRoom = (value) => {
    database.on("value", snap => {

      if (snap.hasChild(value)) {
        if (this.mounted) {
          this.props.history.push({
            pathname: value,
            state: { room: value, loggedIn: true }
          });
            // this.setState({ alertCreate: 'Room already exist', createRoom: '' });
        }
      }
      else {
        database.child(value).set([`Chat data: ${value} room`])
          .then(res => {
            if (this.mounted) {
              this.props.history.push({
                pathname: value,
                state: { room: value, loggedIn: true }
              });
            }
          })
      }
    });
  }

  signOut = () => {
    firebase.auth().signOut();
    this.props.history.push('/login');
  }

  selectRoom = (value) => {
    database.child(value).on("value", snap => {
      if (this.mounted) {
        if (snap.exists()) {
          this.props.history.push({
            pathname: value,
            state: { room: value, loggedIn: true }
          })
        }
        else {
          this.setState({ alertJoin: 'Room doesn\'t exist', selectRoom: '' });
        }
      }
    });
  }
  componentWillUnmount() {
    this.mounted = false;
  }

  render() {

    const { alertCreate, alertJoin } = this.state;

    return (
      <div>
        {this.state.loggedIn ?
          <div className={classes.roomContainer}>
            <header style={{ height: '80px', padding: '20px' }}>
              <Button bsStyle='danger' className={classes.btnDanger} onClick={this.signOut}>SignOut</Button>
            </header>
            <h2>Welcome to ChatRoom</h2>
            <form>
              <div className={classes.Room}>
                <h3>Create a ChatRoom</h3>
                <FormControl
                  className={classes.FormControl}
                  type="text"
                  value={this.state.createRoom}
                  placeholder="Enter text"
                  onChange={this.createRoomHandler} />
                <Button bsStyle='success' className={classes.btnSuccess} onClick={() => this.createRoom(this.state.createRoom)}>Create a ChatRoom</Button><br />
                {alertCreate ? <Alert bsStyle='danger' className={classes.alertDanger} >{alertCreate}</Alert> : null}
              </div>
              <div className={classes.Room}>
                <h3>Join a ChatRoom</h3>
                <FormControl
                  type="text"
                  className={classes.FormControl}
                  value={this.state.selectRoom}
                  placeholder="Enter text"
                  onChange={this.selectRoomHandler} />
                <Button bsStyle='success' className={classes.btnSuccess} onClick={() => this.selectRoom(this.state.selectRoom)}>Select a ChatRoom</Button><br />
                {alertJoin ? <Alert bsStyle='danger' className={classes.alertDanger} >{alertJoin}</Alert> : null}
              </div>
            </form></div> : null}

      </div>
    );
  }
}

export default Home;