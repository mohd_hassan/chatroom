import React, { Component } from 'react';

import Login from './components/Login/Login';
import Home from './components/Home/home';
import { Route, Switch, Redirect } from "react-router-dom";
import messageBox from './components/Home/messageBox';

class App extends Component {


  render() {
    return (
      <div className="App">
        <Switch>
          <Route exact path='/' render={() => <Redirect to='login'/>} />
          <Route exact path='/login' component={Login} />
          <Route exact path='/home' component={Home} />
          <Route exact path='/:id' component={messageBox} />
        </Switch>
      </div>
    );
  }
}

export default App;
