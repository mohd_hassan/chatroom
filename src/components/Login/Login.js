import React, { Component } from 'react';

import firebase from 'firebase';
import firebaseApp from '../../firebase/firebase';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import { Redirect } from "react-router-dom";
import classes from './Login.css';

class login extends Component {

  state = {
    SignedIn: false
  }

  uiConfig = {
    signInFlow: "popup",
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID
    ],
    callbacks: {
      signInSuccessWithAuthResult: () => false
    }
  }

  componentWillMount() {
    this.mounted = true;
    firebaseApp.auth().onAuthStateChanged(user => {
      // if (this.mounted) {
        this.setState({ SignedIn: !!user });
      // }
    });
  }

  // componentWillUnmount(){
  //   this.mounted = false;
  // }

  render() {
    return (
      <div className={classes.Login}>
        {this.state.SignedIn ? <Redirect to='/home' /> :
          <div className={classes.loginBox}>
            <div className={classes.box}>
              <h2 style={{ color: 'black', fontWeight: 'bold' }}>Welcome to ChatRoom</h2>
              <StyledFirebaseAuth className={classes.box} uiConfig={this.uiConfig} firebaseAuth={firebase.auth()} />
            </div>
          </div>}
      </div>
    );
  }
}

export default login;