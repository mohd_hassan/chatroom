import React from 'react';

import classes from './spinnerMsg.css';

const spinner = () => (<div className={classes.Loader}>Loading...</div>);

export default spinner;